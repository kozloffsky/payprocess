Welcome to payprocess's documentation!
**************************************


Architecture
************

System has 3 layers: 1. Models that are described below are ORM
classes that are mapped to database. Usig this approach system is not
depend on concrete database, but only on that are supported by ORM. 2.
Serializers are used to convers models to and from web requests data,
make validation of this data. 3. Views that are responsible on
rendering serialized data and handle REST like api requests 4.
Scheduled tasks responsible for transaction validation and processing


Transaction management
======================

When one user want to make payment to other transaction is created
with status *initialized*. It can be edited and eny time.

Next user performs payment submit for processing, and transaction
status changes into *submitted*. After thet user can not edit or
perform any action with this transaction. Also amount of transaction
is being blocked on users account at time before finalization and user
can not see it in balance information. If transaction releases then
this amount releases.

Submitted transactions are processed by system. First system trys to
validate them by configured validators. For example email confirmation
link or sms or other validation/confirmation. After success validation
actual transfer occurs and change amounts of accounts.


Models
******

Models are mapped to actual db using ORM

class payments.models.Account(*args, **kwargs)

   User`s account information model

   Objects of this class contains information about current amount of
   user account

   Fields:
      user(User): link to user model balance(Float): amount of money
      current account has. Default is 0

   exception DoesNotExist

   exception MultipleObjectsReturned

   actual_balance

      Shows actual amount of money available for this account. All
      incoming transactions with statuses different from Finished are
      freezing their amount. In this case actual balance will be equal
      to balance minus sum of amounts in unfinished incoming
      transactions

class payments.models.Transaction(*args, **kwargs)

   Transaction between two accounts

   Object holds transaction information, amount of transaction and its
   status.

   Fields:
      sender(Account): account from where money should be moved
      receiver(Account): account to where money should be moved
      amount(Float): amount of money this transaction is working with
      status(Integer): transaction status, described in
      :transaction_types: created(DateTime): transaction creation date
      updated(DateTime): transaction last update date

   exception DoesNotExist

   exception MultipleObjectsReturned


Tasks
*****

Scheduled tasks that for transaction processing

class payments.management.commands.processtransaction.Command(stdout=None, stderr=None, no_color=False)

   Command that process all verified transactions, and make actual
   transfer from one account to another. Command designed to be
   running by cron or other task scheduler.

   add_arguments(parser)

      Entry point for subclassed commands to add custom arguments.

   handle(*args, **options)

      The actual logic of the command. Subclasses must implement this
      method.

class payments.management.commands.validatetransaction.Command(stdout=None, stderr=None, no_color=False)

   Command that process all submitted transactions. Validation can
   depends on actual validating pattern for concrete service. It can
   be changed or modified for concrete validation needs. Command
   designed to be running by cron or other task scheduler.

   add_arguments(parser)

      Entry point for subclassed commands to add custom arguments.

   handle(*args, **options)

      The actual logic of the command. Subclasses must implement this
      method.


Indices and tables
******************

* Index

* Module Index

* Search Page
