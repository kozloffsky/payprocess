from django.contrib.auth.models import User
from django.db import models


class Account(models.Model):
    """User`s account information model

    Objects of this class contains information about current amount of user account

    Fields:
        user(User): link to user model
        balance(Float): amount of money current account has. Default is 0
    """
    user = models.ForeignKey(User, related_name='account', on_delete=models.DO_NOTHING)
    balance = models.FloatField(default=0, null=False)

    @property
    def actual_balance(self):
        """Shows actual amount of money available for this account.
        All incoming transactions with statuses different from Finished are freezing their amount.
        In this case actual balance will be equal to balance minus sum of amounts in unfinished incoming transactions
        """
        return self.balance


"""Transaction statuses
    Initialized - this is first state, when sender not yet submitted transaction for processing and can be edited of 
    removed at any time safely
    Submitted - transaction is submitted for verification and become read only
    Verified - transaction verification process is finished and now system can complete this transaction
    Finished - transaction complete, amount is transfered from sender to receiver successfully.
    NotVerified - verification fails so transaction can not be processed. Manually can be marked for verification retry
"""

INITIALIZED = 0
SUBMITTED = 1
VERIFIED = 2
FINISHED = 3
NOT_VERIFIED = 4

transaction_statuses = (
    (INITIALIZED, 'Initialized'),
    (SUBMITTED, 'Submitted'),
    (VERIFIED, 'Verified'),
    (FINISHED, 'Finished'),
    (NOT_VERIFIED, 'NotVerified')
)


class Transaction(models.Model):
    """ Transaction between two accounts

    Object holds transaction information, amount of transaction and its status.

    Fields:
        sender(Account): account from where money should be moved
        receiver(Account): account to where money should be moved
        amount(Float): amount of money this transaction is working with
        status(Integer): transaction status, described in :transaction_types:
        created(DateTime): transaction creation date
        updated(DateTime): transaction last update date
    """
    sender = models.ForeignKey(Account,related_name='transactions', on_delete=models.DO_NOTHING)
    receiver = models.ForeignKey(Account, related_name='incoming_transactions', on_delete=models.DO_NOTHING)
    amount = models.FloatField(null=False, blank=False)
    status = models.IntegerField(default=0, blank=False, null=False, choices=transaction_statuses)
    created = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated = models.DateTimeField(auto_now_add=True, auto_now=True)
