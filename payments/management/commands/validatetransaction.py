from django.core.management.base import BaseCommand

from payments.models import Transaction, SUBMITTED, VERIFIED


class Command(BaseCommand):
    """
    Command that process all submitted transactions. Validation can depends on actual validating pattern for concrete
    service. It can be changed or modified for concrete validation needs.
    Command designed to be running by cron or other task scheduler.
    """


    help = "Validates all submitted transactions"

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        for transaction in Transaction.objects.get(status=SUBMITTED):
            #here actual verification goes
            transaction.status = VERIFIED
            transaction.save()
