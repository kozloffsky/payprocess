from django.core.management.base import BaseCommand

from payments.models import Transaction, VERIFIED, FINISHED


class Command(BaseCommand):
    """
    Command that process all verified transactions, and make actual transfer from one account to another.
    Command designed to be running by cron or other task scheduler.
    """

    help = "Process all verified transactions"

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        for transaction in Transaction.objects.get(status=VERIFIED):
            #here goes actual transfer from one sender account to receiver
            transaction.status = FINISHED
            transaction.save()
