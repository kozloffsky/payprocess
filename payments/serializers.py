from rest_framework import serializers

from payments.models import Account, Transaction


class AccountSerializer(serializers.ModelSerializer):
    """
    Serializer that converts Account model into fortmat that can be shown from API
    """
    class Meta:
        model = Account
        fields = ('id', 'user', 'amount')


class TransactionSerializer(serializers.ModelSerializer):
    """
    Serializer that converts Transaction model into fortmat that can be shown from API
    """
    class Meta:
        model = Transaction
        fields = ('id', 'status', 'amount', 'sender', 'receiver')
